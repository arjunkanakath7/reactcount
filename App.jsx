import { useState } from 'react';
import './App.css'

function App() {
    const [number,setnumber]= useState(0) 

    function counted(){
      setnumber(number + 1)
    }

  return (
    <>
      <header></header>
      <main>
        <h1>{number}</h1>
        <button onClick={counted}>count</button>
      </main>
      <footer></footer>
    </>
  )
}

export default App
